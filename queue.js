let collection = [];

// Write the queue functions below.

// USAGE OF JS METHODS (push, pop, shift, unshift, etc) IS NOT ALLOWED
// Properties (like .length) are allowed

// Output all the elements of the queue
function print() {

    return collection;
    
}

// Add element to the rear of the queue
function enqueue(element) {

    collection[collection.length] = element;

    return collection;
}

// Remove element at front of queue
function dequeue() {

    for (let i = 0; i < collection.length; i++) {
        collection[i] = collection[i + 1];
    }
    collection.length--;

    return collection;
}

// 5. Show element at the front
function front() {

    return collection[0];
}

function size() {
    return collection.length;
}

function isEmpty() {
    if (collection.length === 0) {
        return true
    } else {
        return false
    }
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};